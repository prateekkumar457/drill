function problem3(data){
    let ans=[];
    for(i=0; i<data.length; i++){
        ans[i]=data[i].car_model;
    }
    ans.sort();
    return ans;
}

module.exports=problem3;